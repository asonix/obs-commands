use crate::persist::ParseError;
use sled::transaction::TransactionError;
use tracing_error::SpanTrace;

#[derive(Debug)]
pub struct Error {
    kind: ErrorKind,
    context: SpanTrace,
}

#[derive(Debug, thiserror::Error)]
pub enum ErrorKind {
    #[error(transparent)]
    Obws(#[from] obws::Error),

    #[error(transparent)]
    Sled(#[from] sled::Error),

    #[error(transparent)]
    Json(#[from] serde_json::Error),

    #[error(transparent)]
    PathBuilding(#[from] ParseError),

    #[error("Authentication required to connect")]
    AuthRequired,

    #[error("Command not found")]
    Missing,

    #[error("Cannot remove command currently use")]
    CommandInUse,
}

impl Error {
    pub fn kind(&self) -> &ErrorKind {
        &self.kind
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{}", self.kind)?;
        std::fmt::Display::fmt(&self.context, f)
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        self.kind.source()
    }
}

impl From<TransactionError<Error>> for Error {
    fn from(txerr: TransactionError<Error>) -> Self {
        match txerr {
            TransactionError::Abort(e) => e,
            TransactionError::Storage(e) => e.into(),
        }
    }
}

impl<T> From<T> for Error
where
    ErrorKind: From<T>,
{
    fn from(t: T) -> Self {
        Error {
            kind: ErrorKind::from(t),
            context: SpanTrace::capture(),
        }
    }
}
