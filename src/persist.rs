use crate::{
    command::CommandList,
    error::{Error, ErrorKind},
};
use path_gen::PathGen;
use sled::{transaction::ConflictableTransactionError, Db, Tree};

mod path;

use path::{CommandField, CommandName, DeckId, Key, NameField, UsedBy, UsedByField, PATH_ROOT};

pub use path::{
    CommandFieldParseError, NameFieldParseError, ParseError, UsedByFieldParseError,
    UsedByParseError,
};

#[derive(Clone)]
pub struct Store {
    commands: Tree,
    _db: Db,
}

impl Store {
    pub fn build(db: Db) -> Result<Self, Error> {
        let commands = db.open_tree("obs-commands.commands-store")?;

        // read everything to memory
        for res in commands.iter() {
            let _ = res?;
        }

        Ok(Store { commands, _db: db })
    }

    pub fn save_command(
        &self,
        command_name: &str,
        command_list: &CommandList,
    ) -> Result<(), Error> {
        let v = serde_json::to_vec(command_list)?;

        let command_name_path = PATH_ROOT
            .push(CommandName::new(command_name))
            .field(CommandField)
            .to_bytes();

        self.commands.insert(command_name_path, v)?;

        Ok(())
    }

    pub fn get_command(&self, command_name: &str) -> Result<Option<CommandList>, Error> {
        let command_name_path = PATH_ROOT
            .push(CommandName::new(command_name))
            .field(CommandField)
            .to_bytes();

        if let Some(ivec) = self.commands.get(command_name_path)? {
            return Ok(Some(serde_json::from_slice(&ivec)?));
        }

        Ok(None)
    }

    pub fn map_command(&self, deck_id: &str, key: u8, command_name: &str) -> Result<(), Error> {
        let command_key = PATH_ROOT
            .push(CommandName::new(command_name))
            .field(CommandField)
            .to_bytes();

        let key_command_path = PATH_ROOT
            .push(DeckId::new(deck_id))
            .push(Key::new(key))
            .field(CommandField)
            .to_bytes();

        let command_use_path = PATH_ROOT
            .push(CommandName::new(command_name))
            .push(UsedBy)
            .push(DeckId::new(deck_id))
            .push(Key::new(key))
            .field(UsedByField)
            .to_bytes();

        #[cfg(not(release))]
        let key_command_parser = PathGen::parser()
            .push::<DeckId>()
            .push::<Key>()
            .field::<CommandField>();

        #[cfg(not(release))]
        let command_use_parser = PathGen::parser()
            .push::<CommandName>()
            .push::<UsedBy>()
            .push::<DeckId>()
            .push::<Key>()
            .field::<UsedByField>();

        self.commands.transaction(|commands| {
            if commands.get(&command_key)?.is_none() {
                return Err(trans_err(ErrorKind::Missing));
            }

            if let Some(previous_command_use_path) = commands.get(&key_command_path)? {
                #[cfg(not(release))]
                let previous_command_use_path = command_use_parser
                    .parse(&previous_command_use_path)
                    .map_err(trans_parse_err)?
                    .to_bytes();

                if let Some(previous_key_command_path) =
                    commands.remove(previous_command_use_path)?
                {
                    #[cfg(not(release))]
                    let previous_key_command_path = key_command_parser
                        .parse(&previous_key_command_path)
                        .map_err(trans_parse_err)?
                        .to_bytes();

                    commands.remove(previous_key_command_path)?;
                }
            }

            commands.insert(key_command_path.as_slice(), command_use_path.as_slice())?;
            commands.insert(command_use_path.as_slice(), key_command_path.as_slice())?;
            Ok(())
        })?;

        Ok(())
    }

    // TODO: Figure out transactions for this
    pub fn remove_command(&self, command_name: &str) -> Result<(), Error> {
        if !self.get_command_uses(command_name).is_empty() {
            return Err(ErrorKind::CommandInUse.into());
        }

        let command_key = PATH_ROOT
            .push(CommandName::new(command_name))
            .field(CommandField)
            .to_bytes();

        self.commands.remove(command_key)?;

        Ok(())
    }

    pub fn get_command_uses(&self, command_name: &str) -> Vec<(String, u8)> {
        let command_use_prefix = PATH_ROOT
            .push(CommandName::new(command_name))
            .prefix::<UsedBy>()
            .to_bytes();

        let key_command_parser = PathGen::parser()
            .push::<DeckId>()
            .push::<Key>()
            .field::<CommandField>();

        self.commands
            .scan_prefix(&command_use_prefix)
            .values()
            .filter_map(|res| res.ok())
            .filter_map(|key_command_map| {
                let key_command_path = key_command_parser
                    .parse::<ParseError>(&key_command_map)
                    .ok()?;

                let key = key_command_path.parent().key();
                let deck_id = key_command_path.parent().parent().deck_id().to_string();

                Some((deck_id, key))
            })
            .collect()
    }

    pub fn get_mapped_command(&self, deck_id: &str, key: u8) -> Result<Option<String>, Error> {
        let key_command_path = PATH_ROOT
            .push(DeckId::new(deck_id))
            .push(Key::new(key))
            .field(CommandField)
            .to_bytes();

        let command_use_parser = PathGen::parser()
            .push::<CommandName>()
            .push::<UsedBy>()
            .push::<DeckId>()
            .push::<Key>()
            .field::<UsedByField>();

        if let Some(ivec) = self.commands.get(key_command_path)? {
            let command_use_path = command_use_parser.parse::<ParseError>(&ivec)?;

            let command_name = command_use_path
                .parent()
                .parent()
                .parent()
                .parent()
                .command_name()
                .to_string();

            return Ok(Some(command_name));
        }

        Ok(None)
    }

    pub fn mappings_for_deck(&self, deck_id: &str) -> Vec<(u8, String)> {
        let key_prefix_for_deck = PATH_ROOT
            .push(DeckId::new(deck_id))
            .prefix::<Key>()
            .to_bytes();

        let command_use_parser = PathGen::parser()
            .push::<CommandName>()
            .push::<UsedBy>()
            .push::<DeckId>()
            .push::<Key>()
            .field::<UsedByField>();

        self.commands
            .scan_prefix(key_prefix_for_deck)
            .values()
            .filter_map(|res| res.ok())
            .filter_map(|v| {
                let command_use_path = command_use_parser.parse::<ParseError>(&v).ok()?;

                let key = command_use_path.parent().key();
                let command_name = command_use_path
                    .parent()
                    .parent()
                    .parent()
                    .parent()
                    .command_name()
                    .to_string();

                Some((key, command_name))
            })
            .collect()
    }

    pub fn save_deck_name(&self, deck_id: &str, name: &str) -> Result<(), Error> {
        let deck_name_path = PATH_ROOT
            .push(DeckId::new(deck_id))
            .field(NameField)
            .to_bytes();

        self.commands.insert(deck_name_path, name.as_bytes())?;
        Ok(())
    }

    pub fn get_deck_name(&self, deck_id: &str) -> Result<Option<String>, Error> {
        let deck_name_path = PATH_ROOT
            .push(DeckId::new(deck_id))
            .field(NameField)
            .to_bytes();

        if let Some(ivec) = self.commands.get(deck_name_path)? {
            let string = String::from_utf8(ivec.to_vec()).map_err(ParseError::from)?;

            return Ok(Some(string));
        }

        Ok(None)
    }

    pub fn save_key_name(&self, deck_id: &str, key: u8, name: &str) -> Result<(), Error> {
        let key_name_path = PATH_ROOT
            .push(DeckId::new(deck_id))
            .push(Key::new(key))
            .field(NameField)
            .to_bytes();

        self.commands.insert(key_name_path, name.as_bytes())?;
        Ok(())
    }

    pub fn get_key_name(&self, deck_id: &str, key: u8) -> Result<Option<String>, Error> {
        let key_name_path = PATH_ROOT
            .push(DeckId::new(deck_id))
            .push(Key::new(key))
            .field(NameField)
            .to_bytes();

        if let Some(ivec) = self.commands.get(key_name_path)? {
            let string = String::from_utf8(ivec.to_vec()).map_err(ParseError::from)?;

            return Ok(Some(string));
        }

        Ok(None)
    }

    pub fn all_commands(&self) -> Vec<(String, CommandList)> {
        let command_prefix = PATH_ROOT.prefix::<CommandName>().to_bytes();
        let command_parser = PathGen::parser()
            .push::<CommandName>()
            .field::<CommandField>();

        self.commands
            .scan_prefix(command_prefix)
            .filter_map(|res| res.ok())
            .filter_map(|(k, v)| {
                let command_name_path = command_parser.parse::<ParseError>(&k).ok()?;
                let command_list = serde_json::from_slice(&v).ok()?;

                let command_name = command_name_path.parent().command_name().to_string();

                Some((command_name, command_list))
            })
            .collect()
    }
}

impl std::fmt::Debug for Store {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Store").finish()
    }
}

#[cfg(not(release))]
fn trans_parse_err(e: ParseError) -> ConflictableTransactionError<Error> {
    trans_err(e.into())
}

fn trans_err(kind: ErrorKind) -> ConflictableTransactionError<Error> {
    ConflictableTransactionError::Abort(ErrorKind::from(kind).into())
}

#[cfg(test)]
mod tests {
    use crate::{
        command::{CommandList, GetSceneList},
        persist::Store,
    };
    use std::convert::TryInto;

    fn with_store<O>(f: impl Fn(Store) -> O) -> O {
        let db = sled::Config::new().temporary(true).open().unwrap();
        let store = Store::build(db).unwrap();

        (f)(store)
    }

    #[test]
    fn store_and_retrieve_command() {
        let cmd = CommandList::new(GetSceneList::new());
        let name = "store_and_retrieve_command";

        let new_cmd = with_store(|store| {
            store.save_command(name, &cmd).unwrap();
            store.get_command(name).unwrap().unwrap()
        });

        assert_eq!(cmd, new_cmd);
    }

    #[test]
    fn map_key_to_command() {
        let cmd = CommandList::new(GetSceneList::new());
        let name = "map_key_to_command";
        let key = 1;
        let deck_id = "012345";

        let new_name = with_store(|store| {
            store.save_command(name, &cmd).unwrap();
            store.map_command(deck_id, key, name).unwrap();
            store.get_mapped_command(deck_id, key).unwrap().unwrap()
        });

        assert_eq!(new_name, name)
    }

    #[test]
    fn doesnt_map_missing_command() {
        let name = "doesnt_map_missing_command";
        let key = 1;
        let deck_id = "012345";

        let result = with_store(|store| store.map_command(deck_id, key, name));

        assert!(result.is_err());
    }

    #[test]
    fn adds_use_for_mapped_command() {
        let cmd = CommandList::new(GetSceneList::new());
        let name = "adds_use_for_mapped_command";
        let key = 1;
        let deck_id = "012345";

        let uses = with_store(|store| {
            store.save_command(name, &cmd).unwrap();
            store.map_command(deck_id, key, name).unwrap();
            store.get_command_uses(name)
        });

        assert_eq!(uses.len(), 1);
        assert_eq!(uses[0].0, deck_id);
        assert_eq!(uses[0].1, key);
    }

    #[test]
    fn remove_unused_command() {
        let cmd = CommandList::new(GetSceneList::new());
        let name = "remove_unused_command";

        let opt = with_store(|store| {
            store.save_command(name, &cmd).unwrap();
            store.remove_command(name).unwrap();
            store.get_command(name).unwrap()
        });

        assert!(opt.is_none());
    }

    #[test]
    fn dont_remove_used_command() {
        let cmd = CommandList::new(GetSceneList::new());
        let name = "dont_remove_used_command";
        let deck_id = "012345";
        let key = 1;

        let res = with_store(|store| {
            store.save_command(name, &cmd).unwrap();
            store.map_command(deck_id, key, name).unwrap();
            store.remove_command(name)
        });

        assert!(res.is_err());
    }

    #[test]
    fn get_mappings_for_deck() {
        let names = [
            "get_mappings_for_deck_1",
            "get_mappings_for_deck_2",
            "get_mappings_for_deck_3",
        ];
        let cmds = names
            .iter()
            .map(|name| (*name, CommandList::new(GetSceneList::new())))
            .collect::<Vec<_>>();

        let mappings = names
            .iter()
            .enumerate()
            .map(|(index, name)| {
                let index: u8 = index.try_into().unwrap();
                let key: u8 = index + 1;

                (key, *name)
            })
            .collect::<Vec<_>>();

        let deck_id = "012345";

        let new_mappings = with_store(|store| {
            for (name, cmd) in &cmds {
                store.save_command(name, cmd).unwrap();
            }

            for (key, name) in &mappings {
                store.map_command(deck_id, *key, name).unwrap();
            }

            store.mappings_for_deck(deck_id)
        });

        assert_eq!(new_mappings.len(), mappings.len());

        for (key, name) in new_mappings {
            mappings
                .iter()
                .find(|(left_key, left_name)| *left_key == key && *left_name == name)
                .unwrap();
        }
    }

    #[test]
    fn doesnt_get_mappings_for_wrong_deck() {
        let names = [
            "get_mappings_for_deck_1",
            "get_mappings_for_deck_2",
            "get_mappings_for_deck_3",
        ];
        let cmds = names
            .iter()
            .map(|name| (*name, CommandList::new(GetSceneList::new())))
            .collect::<Vec<_>>();

        let mappings = names
            .iter()
            .enumerate()
            .map(|(index, name)| {
                let index: u8 = index.try_into().unwrap();
                let key: u8 = index + 1;

                (key, *name)
            })
            .collect::<Vec<_>>();

        let deck_id = "012345";
        let wrong_deck_id = "543210";

        let new_mappings = with_store(|store| {
            for (name, cmd) in &cmds {
                store.save_command(name, cmd).unwrap();
            }

            for (key, name) in &mappings {
                store.map_command(deck_id, *key, name).unwrap();
            }

            store.mappings_for_deck(wrong_deck_id)
        });

        assert_eq!(new_mappings.len(), 0);
    }

    #[test]
    fn save_and_retrieve_deck_name() {
        let deck_name = "Some Name";
        let deck_id = "012345";

        let new_deck_name = with_store(|store| {
            store.save_deck_name(deck_id, deck_name).unwrap();
            store.get_deck_name(deck_id).unwrap().unwrap()
        });

        assert_eq!(new_deck_name, deck_name);
    }

    #[test]
    fn save_and_retrieve_key_name() {
        let key_name = "Top Left";
        let deck_id = "012345";
        let key = 1;

        let new_key_name = with_store(|store| {
            store.save_key_name(deck_id, key, key_name).unwrap();
            store.get_key_name(deck_id, key).unwrap().unwrap()
        });

        assert_eq!(new_key_name, key_name);
    }

    #[test]
    fn key_name_doesnt_interfere_with_mappings() {
        let names = [
            "get_mappings_for_deck_1",
            "get_mappings_for_deck_2",
            "get_mappings_for_deck_3",
        ];
        let cmds = names
            .iter()
            .map(|name| (*name, CommandList::new(GetSceneList::new())))
            .collect::<Vec<_>>();

        let mappings = names
            .iter()
            .enumerate()
            .map(|(index, name)| {
                let index: u8 = index.try_into().unwrap();
                let key: u8 = index + 1;

                (key, *name)
            })
            .collect::<Vec<_>>();

        let key_names = names
            .iter()
            .enumerate()
            .map(|(index, _)| {
                let index: u8 = index.try_into().unwrap();
                let key: u8 = index + 1;

                (key, format!("Key {}", key))
            })
            .collect::<Vec<_>>();

        let deck_id = "012345";

        let new_mappings = with_store(|store| {
            for (name, cmd) in &cmds {
                store.save_command(name, cmd).unwrap();
            }

            for (key, name) in &mappings {
                store.map_command(deck_id, *key, name).unwrap();
            }

            for (key, name) in &key_names {
                store.save_key_name(deck_id, *key, name).unwrap();
            }

            store.mappings_for_deck(deck_id)
        });

        assert_eq!(new_mappings.len(), mappings.len());

        for (key, name) in new_mappings {
            mappings
                .iter()
                .find(|(left_key, left_name)| *left_key == key && *left_name == name)
                .unwrap();
        }
    }

    #[test]
    fn store_and_retrieve_many_commands() {
        let names = [
            "get_mappings_for_deck_1",
            "get_mappings_for_deck_2",
            "get_mappings_for_deck_3",
        ];
        let cmds = names
            .iter()
            .map(|name| (*name, CommandList::new(GetSceneList::new())))
            .collect::<Vec<_>>();

        let new_commands = with_store(|store| {
            for (name, cmd) in &cmds {
                store.save_command(name, cmd).unwrap();
            }

            store.all_commands()
        });

        assert_eq!(new_commands.len(), 3);

        for (name, command) in new_commands {
            cmds.iter()
                .find(|(left_name, left_cmd)| *left_name == name && *left_cmd == command)
                .unwrap();
        }
    }
}
