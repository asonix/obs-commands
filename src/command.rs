use crate::{
    error::Error,
    response::{Response, ResponseItem, SceneItem},
};
use obws::Client;
use std::time::Duration;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize)]
pub struct CommandList {
    first: Command,
    rest: Vec<CommandNode>,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize)]
struct CommandNode {
    delay: Option<Delay>,
    command: Command,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize)]
struct Delay {
    millis: u64,
    seconds: u64,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize)]
#[serde(tag = "type")]
pub enum Command {
    GetSceneList(GetSceneList),
    SwitchScene(SwitchScene),
    SetSceneItemVisibility(SetSceneItemVisibility),
    SetStreaming(SetStreaming),
    SetRecording(SetRecording),
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize)]
pub struct GetSceneList;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize)]
pub struct SwitchScene {
    to: String,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize)]
pub struct SetSceneItemVisibility {
    scene_name: String,
    item_id: i64,
    operation: VisibilityOperation,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize)]
pub struct SetStreaming {
    operation: StateOperation,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize)]
pub struct SetRecording {
    operation: StateOperation,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize)]
pub enum VisibilityOperation {
    On,
    Off,
    Toggle,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize)]
pub enum StateOperation {
    Start,
    Stop,
    Pause,
    Resume,
    Toggle,
}

impl CommandList {
    pub fn new<C>(command: C) -> Self
    where
        Command: From<C>,
    {
        CommandList {
            first: command.into(),
            rest: Vec::new(),
        }
    }

    pub fn add_command<C>(&mut self, command: C, delay: Option<Duration>) -> &mut Self
    where
        Command: From<C>,
    {
        self.rest.push(CommandNode {
            delay: delay.map(From::from),
            command: command.into(),
        });
        self
    }

    pub async fn execute(&self, client: &Client) -> Result<Response, Error> {
        let mut output = Response::default();

        self.first.execute(&mut output, client).await?;

        for node in &self.rest {
            node.execute(&mut output, client).await?;
        }

        Ok(output)
    }
}

impl CommandNode {
    async fn execute(&self, output: &mut Response, client: &Client) -> Result<(), Error> {
        if let Some(delay) = &self.delay {
            let duration = Duration::from_secs(delay.seconds) + Duration::from_millis(delay.millis);
            tokio::time::sleep(duration).await;
        }

        self.command.execute(output, client).await?;

        Ok(())
    }
}

impl Command {
    async fn execute(&self, output: &mut Response, client: &Client) -> Result<(), Error> {
        match self {
            Command::GetSceneList(get_scene_list) => get_scene_list.execute(output, client).await,
            Command::SwitchScene(switch_scene) => switch_scene.execute(output, client).await,
            Command::SetSceneItemVisibility(set_scene_item_visibility) => {
                set_scene_item_visibility.execute(output, client).await
            }
            Command::SetRecording(set_recording) => set_recording.execute(output, client).await,
            Command::SetStreaming(set_streaming) => set_streaming.execute(output, client).await,
        }
    }
}

impl GetSceneList {
    pub fn new() -> Self {
        GetSceneList
    }

    pub fn into_command(self) -> Command {
        Command::GetSceneList(self)
    }

    async fn execute(&self, output: &mut Response, client: &Client) -> Result<(), Error> {
        let scene_list = client.scenes().get_scene_list().await?;

        output.insert(ResponseItem::CurrentScene {
            name: scene_list.current_scene,
        });
        output.insert(ResponseItem::SceneList {
            scenes: scene_list
                .scenes
                .iter()
                .map(|scene| scene.name.clone())
                .collect(),
        });
        for scene in scene_list.scenes {
            output.insert(ResponseItem::SceneItems {
                scene: scene.name,
                items: scene
                    .sources
                    .into_iter()
                    .map(SceneItem::translate)
                    .collect(),
            });
        }

        Ok(())
    }
}

impl SwitchScene {
    pub fn to(to: String) -> Self {
        SwitchScene { to }
    }

    pub fn into_command(self) -> Command {
        Command::SwitchScene(self)
    }

    async fn execute(&self, output: &mut Response, client: &Client) -> Result<(), Error> {
        client.scenes().set_current_scene(&self.to).await?;

        output.insert(ResponseItem::CurrentScene {
            name: self.to.clone(),
        });

        Ok(())
    }
}

impl SetSceneItemVisibility {
    pub fn show(scene_name: String, item_id: i64) -> Self {
        SetSceneItemVisibility {
            scene_name,
            item_id,
            operation: VisibilityOperation::On,
        }
    }

    pub fn hide(scene_name: String, item_id: i64) -> Self {
        SetSceneItemVisibility {
            scene_name,
            item_id,
            operation: VisibilityOperation::Off,
        }
    }

    pub fn toggle(scene_name: String, item_id: i64) -> Self {
        SetSceneItemVisibility {
            scene_name,
            item_id,
            operation: VisibilityOperation::Toggle,
        }
    }

    pub fn into_command(self) -> Command {
        Command::SetSceneItemVisibility(self)
    }

    async fn execute(&self, _output: &mut Response, client: &Client) -> Result<(), Error> {
        fn to_specification(
            id: i64,
        ) -> either::Either<&'static str, obws::requests::SceneItemSpecification<'static>> {
            either::Either::Right(obws::requests::SceneItemSpecification {
                name: None,
                id: Some(id),
            })
        }

        let visible = match self.operation {
            VisibilityOperation::On => true,
            VisibilityOperation::Off => false,
            VisibilityOperation::Toggle => {
                let properties = client
                    .scene_items()
                    .get_scene_item_properties(
                        Some(&self.scene_name),
                        to_specification(self.item_id),
                    )
                    .await?;

                !properties.visible
            }
        };

        let properties = obws::requests::SceneItemProperties {
            scene_name: Some(&self.scene_name),
            item: to_specification(self.item_id),
            visible: Some(visible),
            ..Default::default()
        };

        client
            .scene_items()
            .set_scene_item_properties(properties)
            .await?;

        Ok(())
    }
}

impl SetRecording {
    pub fn start() -> Self {
        SetRecording {
            operation: StateOperation::Start,
        }
    }

    pub fn stop() -> Self {
        SetRecording {
            operation: StateOperation::Stop,
        }
    }
    pub fn pause() -> Self {
        SetRecording {
            operation: StateOperation::Pause,
        }
    }

    pub fn resume() -> Self {
        SetRecording {
            operation: StateOperation::Resume,
        }
    }

    pub fn toggle() -> Self {
        SetRecording {
            operation: StateOperation::Toggle,
        }
    }

    pub fn into_command(self) -> Command {
        Command::SetRecording(self)
    }

    async fn execute(&self, _output: &mut Response, client: &Client) -> Result<(), Error> {
        let status = client.recording().get_recording_status().await?;

        match self.operation {
            StateOperation::Start if !status.is_recording => {
                client.recording().start_recording().await?;
            }
            StateOperation::Stop if status.is_recording => {
                client.recording().stop_recording().await?;
            }
            StateOperation::Toggle => {
                client.recording().start_stop_recording().await?;
            }
            StateOperation::Pause if !status.is_recording_paused => {
                client.recording().pause_recording().await?;
            }
            StateOperation::Resume if status.is_recording_paused => {
                client.recording().resume_recording().await?;
            }
            _ => (),
        }

        Ok(())
    }
}

impl SetStreaming {
    pub fn start() -> Self {
        SetStreaming {
            operation: StateOperation::Start,
        }
    }

    pub fn stop() -> Self {
        SetStreaming {
            operation: StateOperation::Stop,
        }
    }

    pub fn toggle() -> Self {
        SetStreaming {
            operation: StateOperation::Toggle,
        }
    }

    pub fn into_command(self) -> Command {
        Command::SetStreaming(self)
    }

    async fn execute(&self, _output: &mut Response, client: &Client) -> Result<(), Error> {
        let status = client.streaming().get_streaming_status().await?;

        match self.operation {
            StateOperation::Start if !status.streaming => {
                client.streaming().start_streaming(None).await?;
            }
            StateOperation::Stop if status.streaming => {
                client.streaming().stop_streaming().await?;
            }
            StateOperation::Toggle => {
                client.streaming().start_stop_streaming().await?;
            }
            _ => (),
        }

        Ok(())
    }
}

impl From<Duration> for Delay {
    fn from(duration: Duration) -> Self {
        Delay {
            millis: duration.subsec_millis().into(),
            seconds: duration.as_secs(),
        }
    }
}

impl From<GetSceneList> for Command {
    fn from(gsl: GetSceneList) -> Self {
        gsl.into_command()
    }
}

impl From<SwitchScene> for Command {
    fn from(ss: SwitchScene) -> Self {
        ss.into_command()
    }
}

impl From<SetSceneItemVisibility> for Command {
    fn from(ssiv: SetSceneItemVisibility) -> Self {
        ssiv.into_command()
    }
}

impl From<SetRecording> for Command {
    fn from(sr: SetRecording) -> Self {
        sr.into_command()
    }
}

impl From<SetStreaming> for Command {
    fn from(ss: SetStreaming) -> Self {
        ss.into_command()
    }
}
