use path_gen::{DeconstructError, PathGen};
use std::{borrow::Cow, str::Utf8Error, string::FromUtf8Error};

pub(super) const PATH_ROOT: PathGen<'static> = PathGen::new("obs-commands");

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(super) struct CommandField;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("Invalid command field")]
pub struct CommandFieldParseError;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(super) struct CommandName<'a> {
    inner: Cow<'a, str>,
}

impl<'a> CommandName<'a> {
    pub(super) const fn new(s: &'a str) -> Self {
        CommandName {
            inner: Cow::Borrowed(s),
        }
    }

    fn new_owned(s: String) -> Self {
        CommandName {
            inner: Cow::Owned(s),
        }
    }

    pub(super) fn command_name(&self) -> &str {
        &self.inner
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(super) struct DeckId<'a> {
    inner: Cow<'a, str>,
}

impl<'a> DeckId<'a> {
    pub(super) const fn new(s: &'a str) -> Self {
        DeckId {
            inner: Cow::Borrowed(s),
        }
    }

    fn new_owned(s: String) -> Self {
        DeckId {
            inner: Cow::Owned(s),
        }
    }

    pub(super) fn deck_id(&self) -> &str {
        &self.inner
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(super) struct Key {
    inner: u8,
}

impl Key {
    pub(super) fn new(key: u8) -> Self {
        Key { inner: key }
    }

    pub(super) fn key(&self) -> u8 {
        self.inner
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("Invalid key")]
pub struct ParseKeyError;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(super) struct NameField;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("Invalid name field")]
pub struct NameFieldParseError;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(super) struct UsedBy;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("Invalid UseBy node")]
pub struct UsedByParseError;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(super) struct UsedByField;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("Invalid use-by field")]
pub struct UsedByFieldParseError;

#[derive(Debug, thiserror::Error)]
pub enum ParseError {
    #[error(transparent)]
    Command(#[from] CommandFieldParseError),

    #[error(transparent)]
    Name(#[from] NameFieldParseError),

    #[error(transparent)]
    UseBy(#[from] UsedByParseError),

    #[error(transparent)]
    UseByField(#[from] UsedByFieldParseError),

    #[error(transparent)]
    Key(#[from] ParseKeyError),

    #[error(transparent)]
    Utf8(#[from] Utf8Error),

    #[error(transparent)]
    FromUtf8(#[from] FromUtf8Error),

    #[error(transparent)]
    Path(#[from] DeconstructError),
}

mod implementation {
    use super::{
        CommandField, CommandFieldParseError, CommandName, DeckId, Key, NameField,
        NameFieldParseError, ParseError, ParseKeyError, UsedBy, UsedByField, UsedByFieldParseError,
        UsedByParseError,
    };
    use path_gen::{PathField, PathItem, PathNode};
    use std::{borrow::Cow, str::from_utf8};

    impl<'a> PathItem<'a> for CommandField {
        type Error = CommandFieldParseError;

        fn parse(bytes: Cow<'a, [u8]>) -> Result<Self, Self::Error>
        where
            Self: Sized + 'a,
        {
            if bytes.as_ref() == b"command" {
                return Ok(CommandField);
            }

            Err(CommandFieldParseError)
        }

        fn to_bytes<'b>(&'b self) -> Cow<'b, [u8]> {
            Cow::Borrowed(b"command")
        }
    }

    impl<'a> PathItem<'a> for CommandName<'a> {
        type Error = ParseError;

        fn parse(bytes: Cow<'a, [u8]>) -> Result<Self, Self::Error>
        where
            Self: Sized + 'a,
        {
            match bytes {
                Cow::Borrowed(bytes) => Ok(CommandName::new(from_utf8(bytes)?)),
                Cow::Owned(bytes) => Ok(CommandName::new_owned(String::from_utf8(bytes)?)),
            }
        }

        fn to_bytes<'b>(&'b self) -> Cow<'b, [u8]> {
            self.inner.as_bytes().into()
        }
    }

    impl<'a> PathItem<'a> for DeckId<'a> {
        type Error = ParseError;

        fn parse(bytes: Cow<'a, [u8]>) -> Result<Self, Self::Error>
        where
            Self: Sized + 'a,
        {
            match bytes {
                Cow::Borrowed(bytes) => Ok(DeckId::new(from_utf8(bytes)?)),
                Cow::Owned(bytes) => Ok(DeckId::new_owned(String::from_utf8(bytes)?)),
            }
        }

        fn to_bytes<'b>(&'b self) -> Cow<'b, [u8]> {
            self.inner.as_bytes().into()
        }
    }

    impl<'a> PathItem<'a> for Key {
        type Error = ParseKeyError;

        fn parse(bytes: Cow<'a, [u8]>) -> Result<Self, Self::Error>
        where
            Self: Sized + 'a,
        {
            if bytes.len() != 1 {
                return Err(ParseKeyError);
            }

            Ok(Key::new(bytes[0]))
        }

        fn to_bytes<'b>(&'b self) -> Cow<'b, [u8]> {
            Cow::Owned(vec![self.key()])
        }
    }

    impl<'a> PathItem<'a> for NameField {
        type Error = NameFieldParseError;

        fn parse(bytes: Cow<'a, [u8]>) -> Result<Self, Self::Error>
        where
            Self: Sized + 'a,
        {
            if bytes.as_ref() == b"name" {
                return Ok(NameField);
            }

            Err(NameFieldParseError)
        }

        fn to_bytes<'b>(&'b self) -> Cow<'b, [u8]> {
            Cow::Borrowed(b"name")
        }
    }

    impl<'a> PathItem<'a> for UsedBy {
        type Error = UsedByParseError;

        fn parse(bytes: Cow<'a, [u8]>) -> Result<Self, Self::Error>
        where
            Self: Sized + 'a,
        {
            if bytes.as_ref() == b"by" {
                return Ok(UsedBy);
            }

            Err(UsedByParseError)
        }

        fn to_bytes<'b>(&'b self) -> Cow<'b, [u8]> {
            Cow::Borrowed(b"by")
        }
    }

    impl<'a> PathItem<'a> for UsedByField {
        type Error = UsedByFieldParseError;

        fn parse(bytes: Cow<'a, [u8]>) -> Result<Self, Self::Error>
        where
            Self: Sized + 'a,
        {
            if bytes.as_ref() == b"used-by" {
                return Ok(UsedByField);
            }

            Err(UsedByFieldParseError)
        }

        fn to_bytes<'b>(&'b self) -> Cow<'b, [u8]> {
            Cow::Borrowed(b"used-by")
        }
    }

    impl<'a> PathNode<'a> for CommandName<'a> {
        const NAME: &'static [u8] = b"command";
    }

    impl<'a> PathNode<'a> for DeckId<'a> {
        const NAME: &'static [u8] = b"deck-id";
    }

    impl<'a> PathNode<'a> for Key {
        const NAME: &'static [u8] = b"key";
    }

    impl<'a> PathNode<'a> for UsedBy {
        const NAME: &'static [u8] = b"used";
    }

    impl<'a> PathField<'a> for CommandField {}
    impl<'a> PathField<'a> for NameField {}
    impl<'a> PathField<'a> for UsedByField {}

    #[cfg(test)]
    mod tests {
        use path_gen::test::{test_field, test_pathnode};

        use super::{CommandField, CommandName, DeckId, Key, NameField, UsedBy, UsedByField};

        #[test]
        fn test_path_components() {
            test_pathnode::<_, DeckId, _>(DeckId::new("test"));
            test_pathnode::<_, CommandName, _>(CommandName::new("test"));
            test_pathnode::<_, Key, _>(Key::new(5));
            test_pathnode::<_, UsedBy, _>(UsedBy);

            test_field::<_, CommandField, _>(CommandField);
            test_field::<_, NameField, _>(NameField);
            test_field::<_, UsedByField, _>(UsedByField);
        }
    }
}
