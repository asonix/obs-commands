mod command;
mod error;
mod persist;
mod response;

pub use command::{
    Command, CommandList, GetSceneList, SetRecording, SetSceneItemVisibility, SetStreaming,
    SwitchScene,
};
pub use error::{Error, ErrorKind};
pub use persist::Store;
pub use response::{Response, ResponseItem, SceneItem};

use obws::Client;

pub async fn connect(host: &str, port: u16, password: Option<&str>) -> Result<Client, Error> {
    let mut client = Client::connect(host, port).await?;

    if let Some(password) = password {
        client.login(Some(password)).await?;
    } else {
        let auth_required = client.general().get_auth_required().await?;
        if auth_required.auth_required {
            client.disconnect().await;
            return Err(ErrorKind::AuthRequired.into());
        }
    }

    Ok(client)
}
