#[derive(Debug, Default, serde::Deserialize, serde::Serialize)]
pub struct Response {
    items: Vec<ResponseItem>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
#[serde(tag = "type")]
pub enum ResponseItem {
    CurrentScene {
        name: String,
    },
    SceneList {
        scenes: Vec<String>,
    },
    SceneItems {
        scene: String,
        items: Vec<SceneItem>,
    },
}

impl Response {
    pub(crate) fn insert(&mut self, item: ResponseItem) {
        self.items.push(item);
    }
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct SceneItem {
    name: String,
    id: i64,
    ty: String,
    visible: bool,
    children: Vec<SceneItem>,
}

impl SceneItem {
    pub(crate) fn translate(scene_item: obws::common::SceneItem) -> Self {
        SceneItem {
            name: scene_item.name,
            id: scene_item.id,
            ty: scene_item.ty,
            visible: scene_item.render,
            children: scene_item
                .group_children
                .into_iter()
                .map(Self::translate)
                .collect(),
        }
    }
}
